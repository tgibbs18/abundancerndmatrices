library(ggplot2)
library(reshape2)
library(dplyr)
library(gridExtra)
source("MatrixBuilder.R")
#source("PredictionTable.R")
mytheme <- theme_bw() + theme(
  legend.title  = element_text( size=17),
  #  legend.position = "bottom",
  #	legend.direction = "horizontal",
  legend.key = element_blank(),
  legend.text  = element_text( size=17),
  panel.background = element_blank(),
  panel.grid = element_blank(),
  text = element_text( family="Helvetica", size=19),
  panel.border = element_rect( colour = "black", size=2.5),
  axis.ticks = element_line(size = 2.),
  strip.background = element_rect( fill = "transparent", size = 2.5, colour = "black"  ),
  strip.text = element_text(size = 19)
)

PlotNonCorr <- function(Title,
                        res = 150,
                        Distribution_A = "Normal",
                        S = 500,
                        sigmax = 1,
                        sigmay = 1,
                        Distribution_X = "Uniform",
                        mu_X = 0.5,
                        sigma_X = 0.5,
                        shape = 1.1,
                        scale = 1,
                        theta = sqrt(pi/2),
                        mu_D = -1) {

  M <- BuildFinalMatrix_M(S = S,
                          Distribution_A = Distribution_A,
                          mux = 0,
                          muy = 0,
                          sigmax = sigmax,
                          sigmay = sigmay,
                          Distribution_D = "Uniform",
                          mu_D = mu_D,
                          sigma_D = 0,
                          Distribution_X = Distribution_X,
                          mu_X = mu_X,
                          sigma_X = sigma_X,
                          shape = shape,
                          scale = scale,
                          theta = theta)

  eM <- eigen(M, only.values = TRUE)$values
  deM <- data.frame(Real = Re(eM), Imaginary = Im(eM))
  ColorM <- "black"
  ScaleForCartesian <- 1.25
  
  x<-seq(-20, 20, length = res)
  y<-seq(-2, 2, length = res)
  suppData <- data.frame(numeric(res * res), numeric(res * res), numeric(res * res))
  colnames(suppData) <- c("Real", "Imaginary", "zval")
  
  for (i in 1:length(x)) {
    for (j in 1:length(y)) {
        suppData$Real[j + length(y)*(i-1)] <- x[i]
        suppData$Imaginary[j + length(y)*(i-1)] <- y[j]
        suppData$zval[j + length(y)*(i-1)] <- support(a = x[i],
                                                      b = y[j],
                                                      Distribution_X = Distribution_X,
                                                      sigmax = sigmax,
                                                      mu_X = mu_X,
                                                      sigma_X = sigma_X,
                                                      shape = shape,
                                                      scale = scale,
                                                      theta = theta,
                                                      mu_D = mu_D)
    }
  }

  plot <- ggplot(suppData, aes(x = -Real, y = Imaginary, z = zval)) +
    ggtitle(Title) +
    stat_contour(breaks = 0, size = 1.5)
  plot <- plot + geom_point(data = deM, aes(x = -Real, y = Imaginary, z = 0), col = ColorM, shape = 4, alpha = 0.5) +
    theme_bw() +
    theme(plot.margin = unit(c(0.1, 0.1, 0.1, 0.1), "cm")) +
    scale_x_log10()
  return(plot)
}

resolution <- 150


# Standard normal for A. Uniform on (0.25, 1.75) for X. D fixed at -2.
plot1 <- PlotNonCorr(Title = "Uniform",
                   S = 1000,
                   sigma_X = 0.75,
                   mu_X = 1,
                   res = resolution,
                   mu_D = -2)
# Standard Normal distribution for A. LogNormal distribution for X with
# log-mean 0.1 and log-sd 0.5. D fixed at -2.
plot2 <- PlotNonCorr(Title = "Log Normal",
                   S = 1000,
                   res = resolution,
                   Distribution_X = "LogNormal",
                   mu_X = 0.5,
                   sigma_X = 0.5,
                   mu_D = -2)
# # Standard Normal distribution for A. Gamma distribution for X with
# # shape 1.1 and scale  1. D fixed at -2.
# pl3 <- PlotNonCorr(Title = "Gamma",
#                    res = resolution,
#                    Distribution_X = "Gamma",
#                    shape = 2,
#                    mu_D = -2)
# Standard Normal distribution for A. HalfNormal distribution for X with
# theta = 1 and non zero on (1, Inf). D fixed at -2.
plot4 <- PlotNonCorr(Title = "Half Normal",
                   S = 1000,
                   res = resolution,
                   sigmax = 1,
                   sigmay = 1,
                   Distribution_X = "HalfNorm",
                   theta = 1,
                   mu_D = -2)

plotCauchy <- PlotNonCorr(Title = "Cauchy",
                     S = 1000,
                     res = resolution,
                     sigmax = 1,
                     sigmay = 1,
                     Distribution_X = "Cauchy",
                     theta = 1,
                     mu_D = -2)



eigPlot <- function(eigTable, legend = "none") {
  
  eigTable$SDA <- as.factor(eigTable$SDA)
  eigTable$shape <- c(rep(15, times = 20), rep(16, times = 20), rep(17, times = 20))
  
  pl <- ggplot(eigTable, aes(x = Act, y = Pred, group = SDA)) +
    geom_point(aes(shape = shape, color = SDA, alpha = 0.75, size = 1)) +
    geom_abline(intercept = 0, slope = 1) +
    scale_shape_identity() +
    xlab("Leading Eigenvalue") +
    ylab("Predicted Leading Eigenvalue") +
    theme_bw() +
    theme(legend.position = legend, axis.title = element_text(size = 8))

  return(pl)
}

mytheme <- theme_bw() + theme(
  legend.title  = element_text( size=10),
  #	legend.position = "bottom",
  #	legend.direction = "horizontal",
  legend.key = element_blank(),
  legend.text  = element_text( size=10),
  panel.background = element_blank(),
  #panel.grid = element_blank(),
  text = element_text( size=10),
  #panel.border = element_rect( colour = "black", size=2),
  axis.ticks = element_line(size = 1.)
)



eigTable1 <- read.csv("eigTable1.csv")
eigTable1 <- aggregate(eigTable1, by = list(eigTable1$SDX, eigTable1$SDA), FUN = mean)
eigTable1$Dist <- as.factor("Uniform")
eigTable2 <- read.csv("eigTable2.csv")
eigTable2 <- aggregate(eigTable2, by = list(eigTable2$SDX, eigTable2$SDA), FUN = mean)
eigTable2$Dist <- as.factor("LogNormal")
# eigTable3 <- read.csv("eigTable3.csv")
eigTable4 <- read.csv("eigTable4.csv")
eigTable4 <- aggregate(eigTable4, by = list(eigTable4$SDX, eigTable4$SDA), FUN = mean)
eigTable4$Dist <- as.factor("HalfNormal")

eigTable <- rbind(eigTable1, eigTable2, eigTable4)
eigTable$SDA <- as.factor(eigTable$SDA)

# eigScatter1 <- eigPlot(aggregate(eigTable1, by = list(eigTable1$SDX, eigTable1$SDA), FUN = mean))
# eigScatter2 <- eigPlot(aggregate(eigTable2, by = list(eigTable2$SDX, eigTable2$SDA), FUN = mean))
# eigScatter3 <- eigPlot(aggregate(eigTable3, by = list(eigTable3$SDX, eigTable3$SDA), FUN = mean))
# eigScatter4 <- eigPlot(aggregate(eigTable4, by = list(eigTable4$SDX, eigTable4$SDA), FUN = mean))

scatterPlots <- ggplot(eigTable) + mytheme +
  aes(x = Act, y = Pred, colour = SDA, shape = SDA  ) +
  geom_point( alpha = 0.75, size = 5 ) +
  theme(legend.position = "bottom" ) +
  scale_x_continuous( name = "Dominant Eigenvalue" ) +
  scale_y_continuous( name = "Predicted Dominant Eigenvalue" ) +
  scale_fill_manual("SDA") +
  geom_abline(intercept = 0, slope = 1) +
  facet_wrap( ~ Dist )

pdf("Fig4.pdf", width = 10)
grid.arrange(plot1, plot2, plot4, scatterPlots,
             layout_matrix = rbind(c(1, 2, 3),
                                   c(4, 4, 4)),
             nrow = 2, ncol = 3)
dev.off()

#### CAUCHY

PlotNonCorr <- function(Title,
                        res = 150,
                        Distribution_A = "Normal",
                        S = 500,
                        sigmax = 1,
                        sigmay = 1,
                        Distribution_X = "Cauchy",
                        mu_X = 0.5,
                        sigma_X = 0.5,
                        shape = 1.1,
                        scale = 1,
                        theta = sqrt(pi/2),
                        mu_D = -1) {
  
  M <- BuildFinalMatrix_M(S = S,
                          Distribution_A = Distribution_A,
                          mux = 0,
                          muy = 0,
                          sigmax = sigmax,
                          sigmay = sigmay,
                          Distribution_D = "Uniform",
                          mu_D = mu_D,
                          sigma_D = 0,
                          Distribution_X = Distribution_X,
                          mu_X = mu_X,
                          sigma_X = sigma_X,
                          shape = shape,
                          scale = scale,
                          theta = theta)
  
  eM <- eigen(M, only.values = TRUE)$values
  deM <- data.frame(Real = Re(eM), Imaginary = Im(eM))
  ColorM <- "black"
  ScaleForCartesian <- 1.25
  
  x<--10^(seq(0, log10(50), length = res))
  y<-seq(-2, 2, length = res)
  suppData <- data.frame(numeric(res * res), numeric(res * res), numeric(res * res))
  colnames(suppData) <- c("Real", "Imaginary", "zval")
  
  for (i in 1:length(x)) {
    for (j in 1:length(y)) {
      suppData$Real[j + length(y)*(i-1)] <- x[i]
      suppData$Imaginary[j + length(y)*(i-1)] <- y[j]
      suppData$zval[j + length(y)*(i-1)] <- support(a = x[i],
                                                    b = y[j],
                                                    Distribution_X = Distribution_X,
                                                    sigmax = sigmax,
                                                    mu_X = mu_X,
                                                    sigma_X = sigma_X,
                                                    shape = shape,
                                                    scale = scale,
                                                    theta = theta,
                                                    mu_D = mu_D)
    }
  }

  

  plot <- geom_point(data = deM, aes(x = Real, y = Imaginary, z = 0), col = ColorM, shape = 4, alpha = 0.5) +
    theme_bw() +
    theme(plot.margin = unit(c(0.1, 0.1, 0.1, 0.1), "cm")) +
    ggtitle(Title) +
    stat_contour(data = suppData , aes(x = Real, y = Imaginary, z = zval), breaks = 0, size = 1.5) +
    scale_x_continuous(limits = c(-50,0), name = "Real")+
    scale_y_continuous(limits = c(-2.5,2.5), name = "Imaginary")
  return(plot)
}

plotCauchy <- PlotNonCorr(Title = "Cauchy",
                          S = 2500,
                          res = resolution,
                          sigmax = 1,
                          sigmay = 1,
                          Distribution_X = "Cauchy",
                          theta = 1,
                          mu_D = -2)
plotCauchy
ggsave( filename = "../Paper/PRX-2round/Paper/CauchyContour.pdf", plotCauchy, width = 4.1, height = 3.75 )


eigTableC <- read.csv("eigTableCauchy.csv")
CauchyPlot <- ggplot(eigTableC) + mytheme +
  aes(x = Act, y = Pred, colour = SDA, shape = as.factor(SDA)  ) +
  geom_point( alpha = 0.75, size = 5 ) +
  theme(legend.position = "bottom" ) +
  scale_x_continuous( name = "Dominant Eigenvalue" ) +
  scale_y_continuous( name = "Predicted Dominant Eigenvalue" ) +
  scale_fill_manual("SDA") +
  geom_abline(intercept = 0, slope = 1) 
CauchyPlot

