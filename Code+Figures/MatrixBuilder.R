# required packages
require(ggplot2)
require(MASS)
#require(grid)
#require(gridExtra)
#require(igraph)
#require(reshape2)
library(fdrtool)
#library(sads)
library(RColorBrewer)
library(reshape2)


SampleCoefficientsFromBivariate <- function(S, mux, muy, sigmax, sigmay, rho){
  NumPairs <- S * (S - 1) / 2
  mus <- c(mux, muy)
  covariance.matrix <- matrix(c(sigmax^2,
                                rho * sigmax * sigmay,
                                rho * sigmax * sigmay,
                                sigmay^2),
                              2, 2)
  Pairs <- mvrnorm(NumPairs, mus, covariance.matrix)
  return(Pairs)
}

SampleCoefficientsFromFourCorners <- function(S, mux, muy, sigmax, sigmay, rho){
  NumPairs <- S * (S - 1) / 2
  gamma <- (rho + 1) / 2
  PosNeg <- sign(rnorm(NumPairs))
  SwitchSign <- sign(runif(NumPairs, -(1-gamma), gamma))
  Pairs <- cbind(mux + PosNeg * sigmax, muy + PosNeg * SwitchSign * sigmay)
  return(Pairs)
}


BuildMatrix_A <- function(Distribution = "Normal",
                        S = 100,
                        C = 1,
                        mux = 0,
                        muy = 0,
                        sigmax = 0.1,
                        sigmay = 0.1,
                        rhoxy = 0){

  ## Build the pairs from the specified distribution
  if (Distribution == "Normal"){
    Pairs <- SampleCoefficientsFromBivariate(S, mux, muy, sigmax, sigmay, rhoxy)
  } else if (Distribution == "FourCorners") {
      Pairs <- SampleCoefficientsFromFourCorners(S, mux, muy, sigmax, sigmay, rhoxy)
  }
  
  A <- matrix(0, S, S)
  k <- 1
  for (i in 1:(S-1)){
    for (j in (i + 1):S){
        A[j,i] <- Pairs[k,1]
        A[i,j] <- Pairs[k,2]
        k <- k + 1
    }
  }
  return(A)
}

BuildDiagonal_D <- function(Distribution = "Normal",
                           S = 100,
                           mu = -1,
                          sigma = 0.1) {
  if (Distribution == "Normal") {
    Sample <- rnorm(S, mean = mu, sd = sigma)
  } else if (Distribution == "Uniform") {
    # Gives uniform distribution the size of two
    # standard deviations
    Sample <- runif(S, min = mu - sigma, max = mu + sigma)
  }
  D <- diag(Sample, S, S)
  return(D)
  }

BuildPopulation_X <-function(Distribution = "LogNormal",
                S = 100,
                mu = 1,
                sigma = 0.1,
                Prob = 0.5,
                x1 = 1,
                x2 = -1,
                shape = 1.1,
                scale = 1,
                theta = sqrt(pi/2)) {
  if (Distribution == "LogNormal") {
    Sample <- rlnorm(S, meanlog = mu, sdlog = sigma)
    # Takes the log standard deviation
    } else if (Distribution == "Uniform") {
    Sample <- runif(S, min = mu - sigma, max = mu + sigma)
  } else if (Distribution == "Delta") {
    Sample <- rep(x1,S)
    for(i in seq(1,S)) {
        if(runif(1) > Prob) {
            Sample[i] <- x2
        }
    }
  } else if(Distribution == "Gamma") {
    Sample <- rgamma(n = S, shape = shape, scale = scale)
  } else if(Distribution == "HalfNorm") {
    Sample <- rhalfnorm(n = S, theta = theta) + 1
  } else if(Distribution == "PureHalfNorm") {
    Sample <- rhalfnorm(n = S, theta = theta) 
  } else if(Distribution == "Cauchy") {
    Sample <- 1+abs(rcauchy(n = S, location = 0, scale = 1) )
  } 
  X <- diag(Sample, S, S)
  return(X)
}

BuildFinalMatrix_M <- function(Distribution_A = "Normal",
                               S = 500,
                               C = 1,
                               mux = 0,
                               muy = 0,
                               sigmax = 1,
                               sigmay = 1,
                               rhoxy = 0,
                               Distribution_D = "Uniform",
                               mu_D = 0,
                               sigma_D = 0,
                               Distribution_X = "Uniform",
                               mu_X = 0.5,
                               sigma_X = 0.5,
                               x1 = 1,
                               x2 = -1,
                               Prob = 0.5,
                               shape = 1.1,
                               scale = 1,
                               theta = sqrt(pi/2)) {
  X <- BuildPopulation_X(Distribution = Distribution_X,
                         S = S,
                         mu = mu_X,
                         sigma = sigma_X,
                         x1 = x1,
                         x2 = x2,
                         Prob = Prob,
                         shape = shape,
                         scale = scale,
                         theta = theta)
  D <- BuildDiagonal_D(Distribution = Distribution_D,
                       S = S,
                       mu = mu_D,
                       sigma = sigma_D)
  A <- BuildMatrix_A(Distribution = Distribution_A,
                     S = S,
                     C = C,
                     mux = mux / S,
                     muy = muy / S,
                     sigmax = sigmax / sqrt(S),
                     sigmay = sigmay / sqrt(S),
                     rhoxy = rhoxy)
  Y <- D + A
  M <- X %*% Y
  return(M)
  }

PlotM <- function(M, ScaleX = 1.25, ScaleY = 1.25) {
  eM <- eigen(M, only.values = TRUE)$values
  deM <- data.frame(Real = Re(eM), Imaginary = Im(eM))
  ColorM <- "black"
  plM <- ggplot(data = deM, aes(x = Real, y = Imaginary)) +
    geom_point(col = ColorM, alpha = 0.5) +
    theme_bw() +
    theme( 
      panel.grid = element_blank(),
      plot.margin = unit(c(0.1, 0.1, 0.1, 0.1), "cm")  ) +
    coord_cartesian(xlim = ScaleX * range(deM$Real),
                    ylim = ScaleY * range(deM$Imaginary))
  return(plM)
}

GridM <- function(M, Title) {
  hm.palette <- colorRampPalette(rev(brewer.pal(9, 'RdBu')), space='Lab')  
  M.melted <- melt(t(M))
  gridM <- ggplot(M.melted, aes(x = dim(M)[1] - Var1, y = Var2, fill = value)) +
    geom_tile() +
    ggtitle(Title) +
    theme(axis.line=element_blank(),
          axis.text.x=element_blank(),
          axis.text.y=element_blank(),
          axis.ticks=element_blank(),
          axis.title.x=element_blank(),
          axis.title.y=element_blank(),
          panel.grid = element_blank(),
          legend.position="none",
          panel.background=element_blank(),
          panel.border=element_blank(),
          panel.grid.major=element_blank(),
          panel.grid.minor=element_blank(),
          plot.background=element_blank(),
          plot.title = element_text(hjust = 0.5)) +
    coord_equal() +
    scale_fill_gradientn(
                        colours=hm.palette(100), limits = c(-1.42,1.42)
                         )
  return(gridM)
}

findLeadEig <- function(A) {
  return(sort(Re(eigen(A, only.values = TRUE)$values), decreasing = TRUE)[1])
}
