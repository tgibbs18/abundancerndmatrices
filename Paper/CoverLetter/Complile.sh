pdflatex $1.tex
pdflatex $1.tex
bibtex $1
pdflatex $1.tex
pdflatex $1.tex
evince $1.pdf &

rm *.bbl
rm *~
rm *.blg
rm *.log
rm *.aux
rm *.toc

