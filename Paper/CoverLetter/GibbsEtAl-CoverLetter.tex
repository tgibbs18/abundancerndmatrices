\documentclass[11pt,letterpaper]{letter} % Specify the font size (10pt, 11pt and 12pt) and paper size (letterpaper, a4paper, etc)

\usepackage{graphicx} % Required for including pictures
\usepackage{microtype} % Improves typography
\usepackage{gfsdidot} % Use the GFS Didot font: http://www.tug.dk/FontCatalogue/gfsdidot/
\usepackage[T1]{fontenc} % Required for accented characters

% Create a new command for the horizontal rule in the document which allows thickness specification
\makeatletter
\def\vhrulefill#1{\leavevmode\leaders\hrule\@height#1\hfill \kern\z@}
\makeatother

%----------------------------------------------------------------------------------------
%	DOCUMENT MARGINS
%----------------------------------------------------------------------------------------

\textwidth 6.75in
\textheight 9.25in
\oddsidemargin -.25in
\evensidemargin -.25in
\topmargin -1in
\longindentation 0.50\textwidth
\parindent 0.4in

%----------------------------------------------------------------------------------------
%	SENDER INFORMATION
%----------------------------------------------------------------------------------------

\def\Who{Stefano Allesina} % Your name
\def\What{} % Your title
\def\Where{Department of Ecology \& Evolution} % Your department/institution
\def\Address{1101 E 57th} % Your address
\def\CityZip{Chicago IL 60637} % Your city, zip code, country, etc
\def\Email{E-mail: sallesina@uchicago.edu} % Your email address
\def\TEL{Phone: (773) 702-7825} % Your phone number
\def\URL{URL: http://allesinalab.uchicago.edu} % Your URL

%----------------------------------------------------------------------------------------
%	HEADER AND FROM ADDRESS STRUCTURE
%----------------------------------------------------------------------------------------

\address{
\includegraphics[width=1in]{UC-logo.jpg} % Include the logo of your institution
\hspace{5.1in} % Position of the institution logo, increase to move left, decrease to move right
\vskip -1.07in~\\ % Position of the text in relation to the institution logo, increase to move down, decrease to move up
\Large\hspace{1.5in} {\sc University of Chicago} \hfill \normalsize % Second line of institution name, adjust hspace if your logo is wide
\makebox[0ex][r]{\bf {\sc \Who} }\hspace{0.08in} % Print your name and title with a little whitespace to the right
~\\[-0.11in] % Reduce the whitespace above the horizontal rule
\hspace{1.5in}\vhrulefill{1pt} \\ % Horizontal rule, adjust hspace if your logo is wide and \vhrulefill for the thickness of the rule
\hspace{\fill}\parbox[t]{2.85in}{ % Create a box for your details underneath the horizontal rule on the right
\footnotesize % Use a smaller font size for the details
\Where\\ % Your department
\Address\\ % Your address
\CityZip\\ % Your city and zip code
\TEL\\ % Your phone number
\Email\\ % Your email address
\URL % Your URL
}
\hspace{-1.4in} % Horizontal position of this block, increase to move left, decrease to move right
\vspace{-1in} % Move the letter content up for a more compact look
}

%----------------------------------------------------------------------------------------
%	TO ADDRESS STRUCTURE
%----------------------------------------------------------------------------------------

\def\opening#1{\thispagestyle{empty}
{\centering\fromaddress \vspace{0.6in} \\ % Print the header and from address here, add whitespace to move date down
\hspace*{\longindentation}\today\hspace*{\fill}\par} % Print today's date, remove \today to not display it
{\raggedright \toname \\ \toaddress \par} % Print the to name and address
\vspace{0.2in} % White space after the to address
\noindent #1 % Print the opening line
% Uncomment the 4 lines below to print a footnote with custom text
%\def\thefootnote{}
%\def\footnoterule{\hrule}
%\footnotetext{\hspace*{\fill}{\footnotesize\em Footnote text}}
%\def\thefootnote{\arabic{footnote}}
}

%----------------------------------------------------------------------------------------
%	SIGNATURE STRUCTURE
%----------------------------------------------------------------------------------------

\signature{\Who \What} % The signature is a combination of your name and title

\long\def\closing#1{
\vspace{0.1in} % Some whitespace after the letter content and before the signature
\noindent % Stop paragraph indentation
\hspace*{\longindentation} % Move the signature right
\parbox{\indentedwidth}{\raggedright
#1 % Print the signature text
\vskip 0.00in % Whitespace between the signature text and your name
\fromsig}} % Print your name and title

%----------------------------------------------------------------------------------------

\begin{document}

%----------------------------------------------------------------------------------------
%	TO ADDRESS
%----------------------------------------------------------------------------------------

\begin{letter}
{$\;$}

%----------------------------------------------------------------------------------------
%	LETTER CONTENT
%----------------------------------------------------------------------------------------

\opening{Dear Dr.\ Thoennessen,}
\setlength{\parindent}{0cm}

Please find here enclosed the manuscript \textit{The effect of
  population abundances on the stability of large random ecosystems},
by T. Gibbs, J. Grilli, T. Rogers, \& S. Allesina, which we would like
to have considered for publication in \textit{Physical Review X}.

In this manuscript, we use random matrix theory and methods borrowed
from statistical mechanics of disordered systems to attack an
old-standing problem in ecology---the effect of population abundances
on the stability of large ecological communities.  While population
abundances are easy to measure empirically, and extensively modeled
(e.g., Azaele et al., \textit{Review of Modern Physics}, 2016), it is
in fact still debated how they affect the ability of ecosystems to
recover after perturbations.

We show that this problem can be solved using random matrices, and we
derive analytically the spectrum of the Jacobian of the Generalized
Lotka-Volterra equations for an arbitrary population abundance
distribution.  The result is surprising: while population abundances
can affect the typical timescale of relaxation after a perturbation,
they cannot not destabilize the system.

This result has several important implications. First, our result
reconciles different positions in the current debate, by showing that
while population abundances affect time-to-recovery following a
perturbation, they do not affect stability. Second, we show that any
positive fixed point is expected to be stable for large communities.
Finally, while alternative attractors (limit cycles and chaos) are
common in small communities, they become exceedingly unlikely when
many species are interacting---possibly explaining the rarity of chaos
in time series from natural communities.

We seek publication in \textit{Physical Review X} because our results
shed light on the problem of modeling dynamical systems with many
agents, and makes use of new mathematical methods that are widely
applicable.  For instance, the firing rate dynamics of neurons
(Kadmond and Sompolinski, \textit{Physical Review X}, 2015) is
described by equations that are analogous to the ones considered in
this manuscript.  For these reasons, we believe that our manuscript is
of direct interest to the wide readership of \textit{Physical Review
  X}.

Thank you very much for the attention dedicated to our work.
  
  \closing{Sincerely,\\\includegraphics[width=0.2\textwidth]{SteSignature.pdf}}
  
\end{letter}
\end{document}
