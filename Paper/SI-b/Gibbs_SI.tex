
\documentclass[onecolumn,superscriptaddress,aps,pre,linenumbers]{revtex4-1}
\linespread{1.5}

\usepackage{color}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{ulem}


%\usepackage{lscape}

%\usepackage{natbib}

\newcommand{\ds}{\displaystyle}
\newcommand{\rev}[1]{ { \color{red} #1} }

%% make sure you have the nature.cls and naturemag.bst files where
%% LaTeX can find them
\usepackage{lineno}
\usepackage{appendix}


\newcommand{\MM}{Methods}
\newcommand{\SI}{Supplementary Information}
%\usepackage[font={small,it,singlespacing}]{caption}
%\captionsetup{font=small}
%\captionsetup[figure]{labelfont=bf}

%\DeclareMathOperator*{\rlM}{\text{Re}\left( \lambda_{M, 1}
%  \right)}
%\DeclareMathOperator*{\rlU}{\text{Re}\left( \lambda_{\widetilde{M}, 1}
%  \right)}

%\DeclareMathOperator*{\rlM}{\text{Re}\left( \lambda_{M, 1}
%  \right)}
%\DeclareMathOperator*{\rlU}{\text{Re}\left( \lambda_{\widetilde{M}, 1}
%  \right)}
\DeclareMathOperator*{\tr}{\text{tr}}
\DeclareMathOperator*{\rlM}{\text{Re}( \lambda_{M, 1})}
\DeclareMathOperator*{\rlU}{\text{Re}( \lambda_{\widetilde{M}, 1})}
\DeclareMathOperator*{\argmax}{arg\,max}
\newcommand{\mat}[1]{\boldsymbol{#1}}
\newcommand{\quat}[1]{\boldsymbol{#1}}
\newcommand{\matquat}[1]{\tilde{\boldsymbol{#1}}}
\newcommand{\vect}[1]{\boldsymbol{#1}}



\DeclareMathOperator*{\G}{\mathcal{G}}
\DeclareMathOperator*{\ud}{\text{d}}

\renewcommand{\emph}[1]{\textit{#1}}

%% make sure you have the nature.cls and naturemag.bst files where
%% LaTeX can find them

%\usepackage{lscape}

\bibliographystyle{naturemag}



\begin{document}

%\linenumbers

\title{The effect of population abundances on the stability of large random ecosystems\\Supplementary Information}

%% Notice placement of commas and superscripts and use of &
%% in the author list

\author{Theo Gibbs}
\affiliation{Dept.~of Ecology \& Evolution, University of Chicago. Chicago, IL 60637, USA}

\author{Jacopo Grilli}
\affiliation{Dept.~of Ecology \& Evolution, University of Chicago. Chicago, IL 60637, USA}


\author{Tim Rogers}
\affiliation{Centre for Networks and Collective Behaviour,
    Department of Mathematical Sciences, University of
    Bath. Claverton Down Bath BA2 7AY United Kingdom.}

\author{Stefano Allesina}
\affiliation{Dept.~of Ecology \& Evolution, University of Chicago. Chicago, IL 60637, USA}
\affiliation{ Computation Institute, University of Chicago.}
\affiliation{Northwestern Institute on Complex Systems (NICO),
  Northwestern University.}
\maketitle



%\section*{Quaternions}

%When constructing the complex numbers from the real numbers, one defines a variable $i$ to be a root of the equation $x^2 + 1 = 0$. The algebraic structure of $\mathbb{C}$ descends from the equation $i^2 = -1$ and the algebraic structure of $\mathbb{R}$. If instead the symbols $i$, $j$ and $k$ are defined by the relations 
%$$ i^2 = j^2 = k^2 = ijk = -1, $$ the resuting algebraic structure is a 4-dimensional extension of the complex numbers called the quaternions. Quaternionic multiplication is not commutative. A quaternion $\bf q$ can be written 
%$$ \textbf{q} = a + bi + cj + dk = z + wj$$ where $a,b,c,d \in \mathbb{R}$ and $z = a + bi$ and $w = c + di$ are complex. $\textbf{q}$ can also be written in matrix form $$ \textbf{q} = \left( \begin{matrix} z & w \\ \bar{w} & \bar{z} \\ \end{matrix} \right)$$ with $z$ and $w$ as before. The conjugate of a quaternion $\textbf{q} = z + wj$ is defined to be $\bar{\textbf{q}} = z - wj$, so that its inverse is $$ \bf q^{-1} = \frac{\bar{q}}{|q|^2}$$ as in the case for complex numbers. The inverse of $ \bf q $ can also be computed by finding the inverse of the corresponding matrix.

\section{Notation and goals}

We aim to study the spectral density of a matrix $\mat{M}$ of the form $\mat{M = XA}$, where $\mat{X}$ is a positive diagonal matrix and $\mat{A}$ a random matrix with arbitrary distribution.
The diagonal entries of $\mat{X}$ are drawn from an arbitrary distribution with positive support, mean $\mu_X$ and variance $\sigma_X^2$.
The diagonal entries of $\mat{A}$ are drawn from an arbitrary distribution with negative support, mean $\mu_d$ and variance $\sigma_d^2$.
Each pair of off-diagonal entries $(A_{ij}, A_{ji})$ is drawn from a bivariate distribution with identical marginal means $\mu$, variances $\sigma^2$ and correlation $\rho$. 

Let $ \mat  B $ be an $ S \times S $ random matrix with complex eigenvalues $ \lambda_i$ for $i = 1, ... , S$. Its spectral density is defined as 
\begin{equation}
 \varrho(x,y) = \frac{1} {S} \sum_{i=1}^S\delta(x - \Re(\lambda_i))\delta(y - \Im(\lambda_i)) 
\end{equation}
which, in the limit of large $S$, converges to 
\begin{equation}
\varrho(x,y) = \mathbb{E} [\delta(x - \Re(\lambda_i))\delta(y - \Im(\lambda_i)) ] \ ,
\end{equation}
where $\mathbb{E}[\cdot]$ stands for the expectation over matrices in the ensemble.

We introduce the resolvent~\cite{Rogers2010}
\begin{equation}
\G(\quat{q}; \mat{B}) = \frac{1} {S} \sum_{i=1}^S (\lambda_i - \quat{q})^{-1} = \frac{1}{S} \tr\left( \mat{B} - \quat{q} \mat{I} \right)^{-1} \ .
\label{eq:resolvent_finite}
\end{equation}
The variable $\quat{q} = \lambda + \epsilon j$ is a quaternion (see section~\ref{sec:quaternions} for definitions and notation) and the resolvent is a function $\G : \mathbb{H} \to \mathbb{H}$.


The resolvent and the spectral density are related by the following formulas~\cite{Rogers2010}:
\begin{equation}
\G(\quat{q}; \mat{B}) = \int dx \ dy \ \varrho(x,y) (x+iy - \quat{q})^{-1} 
\label{resolvent+spectrum}
\end{equation}
and
\begin{equation}
\varrho\left(x,y \right) =  - \frac {1} {\pi} \lim_{\epsilon \to 0^+} \Re \left( \frac{\partial} {\partial \bar{ \lambda} }  \G(\lambda + \epsilon j; \mat{B})  \right) \biggl|_{\lambda = x+iy} \ ,
\label{spectrum+resolvent}
\end{equation}
where $\frac{\partial} {\partial \bar{ \lambda} }$ is the Wirtinger derivative
\begin{equation}
\frac{\partial }{\partial \bar{\lambda}} := \frac{1}{2} \left( \frac{\partial }{\partial x} + i \frac{\partial }{\partial y} \right) \ .
\end{equation}

\section{Quaternions}
\label{sec:quaternions}

When constructing the complex numbers from the real numbers, one defines a variable $i$ to be a root of the equation $x^2 + 1 = 0$.
The algebraic structure of $\mathbb{C}$ descends from the equation $i^2 = -1$ and the algebraic structure of $\mathbb{R}$.
Similarly, the algebra of quaternions $\mathbb{H}$ can be defined by introducing the symbols $i$, $j$ and $k$ and the relations 
\begin{equation}
i^2 = j^2 = k^2 = ijk = -1 \ .
\end{equation}
From these equations all the multiplication rules can be obtained. In particular, it follows that multiplication in $\mathbb{H}$ is not commutative
(e.g. $ij = -ji$).

A quaternion $\quat{q}$ can be written as 
\begin{equation}
\textbf{q} = a + bi + cj + dk \ ,
\end{equation}
where $a,b,c,d \in \mathbb{R}$.
Equivalently, by introducing the two complex numbers $z = a + bi$ and $w = c + di$ and using $k = ij$, one can write
\begin{equation}
\textbf{q} = z + w j \ .
\end{equation}
Another equivalent way to represent quaternions is to write them in matrix form
\begin{equation}
\textbf{q} = \left( \begin{matrix} z & w \\ \bar{w} & \bar{z} \\ \end{matrix} \right) \ .
\end{equation}
It can be shown that, when written in this form, the multiplication rules of quaternions match the rules of matrix multiplication. In particular,
one has that
\begin{equation}
(z + w j)( u + v j ) = ( z u - w \bar{v} ) + (z v + w \bar{u} ) j \ .
\end{equation}
We also introduce the operation
\begin{equation}
(z + w j) \circ ( u + v j ) =  z u - w v j \ ,
\end{equation}
which, in matrix notation, corresponds to element-by-element multiplication.

The conjugate of a quaternion $\textbf{q} = z + wj$ is defined as $\bar{\quat{q}} = \bar{z} - wj$.
From this definition, one obtains the norm of a quaternion
\begin{equation}
| \quat{q} |^2 \equiv \quat{q} \bar{\quat{q}} = | z|^2 + |w|^2   \ ,
\end{equation}
and the inverse 
\begin{equation}
 \quat{q}^{-1} \equiv \bar{\quat{q}} \frac{1}{|\quat{q}|^2}   \ .
\end{equation}
Moreover, the real part of a quaternion is defined as
\begin{equation}
\Re(\quat{q}) \equiv \bar{\quat{q}} + \quat{q} = \Re(z) = a   \ .
\end{equation}





\section{Bulk and outliers of the spectrum of $\mat{M}$}
\label{sec:mudiff}


In this section, we want to show that the mean of $\mat{A}$ does not affect the bulk of eigenvalues of $\mat{M}$.
We decompose the matrix $\bf{M}$ as 
\begin{equation}
 \mat{M} = \mat{X} (\mat{D} - \mu \mat{I} + \mat{B} + \mu \mat{1} ) 
\label{eq:matrix_decomposed}
\end{equation}
 where $\mat{I}$ is the identity matrix, $\mat{1}$ is a matrix of ones and $\mat D $ is the diagonal matrix consisting of the diagonal entries of $ \mat A $.
Written in this way, $\mat{B}$ is a random matrix with mean zero and null diagonal.
We will show that the bulk of the spectrum of $\mat M$ is equivalent to the bulk of eigenvalues of the matrix $ \mat{J} =  \mat{X} (\mat{D} - \mu \mat{I}+ \mat{B}) $.

Using equation~\ref{eq:matrix_decomposed}, the resolvent of $\mat M$ can be written 
\begin{equation} 
\G(\quat{q}; \mat{M}) = \mathbb{E} [\frac{1} {S} \tr \left( \quat{q} \mat{I} - \mat{X} \mat{D} - \mu \mat{X} - \mat{X} \mat{B} - \mu \mat{X} \mat{1} \right)^{-1}] 
= \mathbb{E} [\frac{1} {S} \tr \left( \quat{q} \mat{I} - \mat{J} - \mu \mat{X} \mat{1} \right)^{-1}].
 \end{equation}

Using the Sherman-Morrison formula, if $\mat{Y}$ and $\mat{Y + Z}$ are invertible matrices and $\mat{Z}$ has rank 1, then
\begin{equation}
(\mat{Y} + \mat{Z})^{-1} = \mat{Y}^{-1} + \frac{1}{1+ \tr (\mat{Z}\mat{Y}^{-1})} \mat{Y}^{-1} \mat{Z} \mat{Y}^{-1} \ . 
\end{equation}
Since $\mu \mat{X} \mat{1}$ has rank one, we have
\begin{equation}
 \left( \quat{q} \mat{I} - \mat{J} - \mu \mat{X}\mat{1} \right)^{-1} = (\textbf{q} \mat{I} - \mat{J})^{-1} +
\frac{1} {1+ \tr (\mu \mat{X}\mat{1} (\textbf{q} \mat{I} - \mat{J})^{-1}) } (\textbf{q} \mat{I} - \mat{J})^{-1} \mu \mat{X}\mat{1} (\textbf{q} \mat{I} - \mat{J})^{-1} \ .
 \label{inverse}
 \end{equation}
By introducing the linear operator $\langle \cdot \rangle$ defined by $\langle \mat {C} \rangle = \frac{1} {S} \tr {\mat{C}} $ for an $S \times S $ matrix $\mat C$,
we obtain
 \begin{equation}
 \G(\quat{q}; \mat{M})  = \G(\quat{q}; \mat{J}) +  \frac{\mu}{1+ S \mu \langle \mat{X}\mat{1} (\textbf{q} \mat{I} - \mat{J})^{-1}) \rangle }
 \langle (\textbf{q} \mat{I} - \mat{J})^{-1} \mat{X} \mat{1} (\textbf{q} \mat{I} - \mat{J})^{-1} \rangle  \ .
  \label{eq:subleading}
\end{equation}
In the limit of large $S$, the contribution from the second term in~\ref{eq:subleading} is subleading. Therefore, the resolvent of $\mat M$ converges to the resolvent of $\mat J$ when $S$ is large.
In other words,  as shown in Figure \ref{fig:M+J}, the bulks of the eigenvalues of $\mat{M}$ and $\mat{J}$ are the same --- up to finite-size corrections.

%We observed numerically (see Figure XXX in the main text), that the
%the spectral density of $\mat M$ is characterized by a bulk of eigenvalues and one one outlier.
%Since the bulk of eigenvalues are determined by $ \mat J $, we account for the contribution of the bulk on $\tr \mat M$ to obtain the prediction $\lambda_{out} = \mu_X (\mu_d + (S - 1)\mu)$ derived in the text.

\begin{figure}
	\includegraphics[scale = .75] {../../Code+Figures/suppFigM+J.pdf}
	\caption{The eigenvalue distributions for $\mat M $ and $ \mat J $ of size $ S = 250 $ with $\mat D $ and $\mat X$  following different distributions. In each case, $\mat A $ is a bivariate normal distribution with identical marginals $ \mu = 2 / S$, $\sigma = 0.5$ and $ \rho = 0$. Uniform D has $\mat D $ following a uniform distribution on $(-1.5, -0.5)$. LogNormal X has $\mat X$ following a log-normal distribution with log-mean $0$ and log-standard deviation $0.35$. HalfNormal X has $ \mat X $ following a half-normal distribution with support $(1, \infty)$ and parameter $\theta = 1$.}
	\label{fig:M+J}
\end{figure}

\section*{The case $\sigma = 0$}

When $\sigma = 0$, we derive the spectrum of a matrix $\mat{Q} = \mat{X} \left(  \mat{D} + \mu \mat{1} \right)$. This case corresponds to
setting $\mat{B} = 0$ in equation~\ref{eq:matrix_decomposed}.
As shown in the main text, the spectral density of the matrix $\mat Q $ is characterized by the presence of an outlier. In this section, we focus on the
bulk of eigenvalues.

% HERE


If we take $ \mat{J} =  \mat{X} ( \mat{D} - \mu \mat{I} + \mat{B}) $ as before and set $\sigma = 0$, then $\mat B = 0 $, so that $\mat {M} = \mat{Q}$ and $ \mat{J} =  \mat{X} ( \mat{D} - \mu \mat{I})$.  The bulk of eigenvalues of $\mat{Q}$ and  $\mat{J}$ will be the same. The resolvent of $\mat{J}$ in the case $\sigma = 0$ reads
\begin{equation}
\begin{aligned}
G(\textbf{q}; \mat{J}) & = \frac{1} {S} \tr \left( \textbf{q} - \mat{J} \right)^{-1} \\ 
& = \frac{1} {S} \tr \left( \textbf{q} - \mat{X}  \mat{D}   \right)^{-1} \\
& = \frac{1} {S} \sum_{i = 1}^S \frac{1} {q - X_i D_i } \\
\label{resolventQ}
\end{aligned}
\end{equation}
since $\textbf{q} - \mat{X} ( \mat{D} - \mu \mat{I})$ is symmetric. In the limit of large $S$, the sum in eq.~\ref{resolventQ} tends toward $ \mathbb{E} \left[(\textbf{q} - \mat{X} \mat{D} )^{-1} \right]$. If $P_{XD}(x,s)$ is the joint distribution of the entries of $\mat{X}$ and $\mat{D}$, we obtain
\begin{equation}
\begin{aligned}
G(\textbf{q}; \mat{J}) & = \frac{1} {S} \sum_{i = 1}^S \frac{1} {q - X_iD_i + \mu X_i} \\
& = \mathbb{E} \left[(\textbf{q} - \mat{X} ( \mat{D} - \mu \mat{I}))^{-1} \right] \\
& = \int dx \ ds \ \frac{P_{XD}(x,s)} {q - x s} \ . 
\label{spectrumQ}
\end{aligned}
\end{equation}
In the case of a constant diagonal matrix $\mat{D} = d \mat{I}$, this equation simplifies to
\begin{equation}
G(\textbf{q}; \mat{J}) = \int d x \ \frac{P_X(x)} {q - x d } 
= \frac{1}{d}\int d y \ \frac{P_X\left(\frac{y}{d} \right)}{q - y}  \\
\end{equation}
with the change of variables $y = x d$. Using equation~\ref{resolvent+spectrum}, we obtain that the spectral density $\varrho_{\mat{J}}(\lambda)$ will be 
\begin{equation}
\varrho_{\mat{J}}(\lambda) =  \frac{1}{d} P_X \left(\frac{\lambda} { d} \right) \ .
\label{predictedQ}
\end{equation}
In Figure \ref{fig:observation}, we plot the prediction from Equation \ref{predictedQ} against the bulk of the spectrum of $\mat Q $ for two distributions of $\mat X$.


\begin{figure}
	\includegraphics[scale = .75] {../../Code+Figures/suppFigSpectrumB.pdf}
	\caption{Histograms of the eigenvalue distribution for two matrices $\mat Q$ of size $ S = 1500$. In each matrix, $\sigma_d = 0$, $\mu_d = -1$ and $ \mu = 5 / S$. The Uniform plot has $\mat{X}$ following a uniform distribution on $(0,1)$ and the LogNormal plot has $\mat{X}$ following a log-normal distribution with log-mean and log-standard deviation both $0.5$.}
	\label{fig:observation}
\end{figure}


\section{Derivation of the spectral density using the cavity method}




In section~\ref{sec:mudiff}, we showed that we can isolate the effect of $\mu \neq 0$. In this section, we use the cavity method~\cite{XXX} to
derive the
spectrum of the matrix $ \mat{J} =  \mat{X} ( \mat{D} - \mu \mat{I} + \mat{B}) $, where $\mat{D}$ and $\mat{X}$ are two random diagonal
matrices and $\mat{B}$ is a random matrix following the elliptic law. 


We introduce the resolvent matrix 
\begin{equation}
\mat{G} = ( \mat{M} - \quat{q} \mat{I}  )^{-1} \ ,
\end{equation}
The resolvent can be written as
\begin{equation}
\G(\quat{q}; \mat{B}) = \frac{1} {S} \tr \mat{G}  \ .
\end{equation}
Note that each element of the resolvent matrix is a quaternion. In particular we will use the notation
\begin{equation}
\quat{G}_{ik} = \alpha_{ik} + \beta_{ik} j \equiv \left( \begin{matrix} \alpha_{ik} & \beta_{ik} \\ \bar{\beta_{ik}} & \bar{\alpha_{ik}} \\ \end{matrix} \right) \ ,
\end{equation}
while $\G = \alpha + \beta j$, where $\alpha = \sum_i \alpha_{ii} / S$ and $\beta = \sum_i \beta_{ii} / S$.
The cavity method allows us to compute the elements of $\mat{G}$ (and therefore the resolvent $\G$) if the matrix $\mat{M}$ has a tree structure~\cite{Rogers2008,Rogers2009}.
It also allows to compute the spectral density for large, densely connected, random matrices~\cite{Rogers2008,Rogers2009,Grilli2016}.
In the limit of large $S$, for a densely connected matrix $\mat{M}$, the cavity equations read~\cite{Rogers2009,Grilli2016}
\begin{equation}
\quat{G}_{il} \equiv 
\left( \begin{matrix} \alpha_{il} & \beta_{il} \\ \bar{\beta}_{il} & \bar{\alpha}_{il} \\ \end{matrix} \right) = 
- \left(
\left( \begin{matrix} \lambda & \epsilon \\ \epsilon & \bar{\lambda} \\ \end{matrix} \right) 
+ \sum_{jk}
\left( \begin{matrix} M_{ij} & 0 \\ 0 & M_{ji} \\ \end{matrix} \right)
\left( \begin{matrix} \alpha_{jk} & \beta_{jk} \\ \bar{\beta}_{jk} & \bar{\alpha}_{jk} \\ \end{matrix} \right)
\left( \begin{matrix} M_{kl} & 0 \\ 0 & M_{lk} \\ \end{matrix} \right)
\right)^{-1}
 \ .
\end{equation}
By introducing 
\begin{equation}
\matquat{M}_{ij} = \left( \begin{matrix} M_{ij} & 0 \\ 0 & M_{ji} \\ \end{matrix} \right)
 \ ,
\end{equation}
we obtain the more compact equation
\begin{equation}
\quat{G}_{il} = -\left(
\quat{q} + \sum_{jk} \matquat{M}_{ij} \quat{G}_{jk} \matquat{M}_{kl} \right)^{-1} \ .
\end{equation}


Our goal is to find the resolvent for a random matrix of the form $\mat{M} = \mat{X} (\mat{D} + \mat{B})$,
where $\mat{X}$ and $\mat{D}$ are diagonal matrices, while $\mat{B}$ is a random matrix following the elliptic law.
We introduce the matrix
\begin{equation}
 \left( \begin{matrix}
-\quat{q} \mat{I} & \matquat{X} \\
- \matquat{D} - \matquat{B} & \mat{I}
\end{matrix} \right) \ ,
\end{equation}
which, when quaternions are represented as $2 \times 2$ matrices, is a 
$4S \times 4S$ matrix. In particular,  this matrix is composed of $S^2$
$4 \times 4$ blocks with entries
\begin{equation}
 \left( \begin{matrix}
-\lambda \delta_{ij} & -\epsilon \delta_{ij} & X_{ii} \delta_{ij} & 0 \\
-\epsilon \delta_{ij} & -\bar{\lambda} \delta_{ij}  & 0 & X_{ii} \delta_{ij} \\
-D_{ii} \delta_{ij} - B_{ij} & 0 &  \delta_{ij} & 0 \\
0 & -D_{ii} \delta_{ij} - B_{ji} & 0 &  \delta_{ij}  
\end{matrix} \right) \ .
\end{equation}

It is simple to observe that
\begin{equation}
 \mat{T} = \left( \begin{matrix}
-\bf{q} & \mat{X} \\
\mat{- A} & \mat{I}
\end{matrix} \right)^{-1} 
= \left( \begin{matrix} 
\left( -\quat{q} \mat{I} + \mat{X} (\mat{D} + \mat{B}) \right)^{-1}  & ... \\
... & ...
\end{matrix} \right)
= \left( \begin{matrix} 
\mat{G} & ... \\
... & ...
\end{matrix} \right) \ .
\end{equation}

If we write the cavity equation for $\mat{T}$, assuming dense matrices, we obtain
\begin{equation}
\quat{T}_{ii} = \left[ \left( \begin{matrix}
-\mat{q}  & \matquat{X}_{ii} \\
- \matquat{D}_{ii} & 1
\end{matrix} \right)
- \sum_{j,k }
\left( \begin{matrix}
\mat{0} & \mat{0} \\
\matquat{B}_{ij} & \mat{0}
\end{matrix} \right)
\mat{T}_{jk}
\left( \begin{matrix}
\mat{0} & \mat{0} \\
\matquat{B}_{ki} & \mat{0}
\end{matrix} \right)
\right]^{-1} \ .
\end{equation}


 We can apply the law of large numbers to take the expectation of the matrices over $\mat{B}$.
Using the following expectations over the elements of $\mat{B}$
\begin{equation}
\begin{split}
\mathbb{E}\left[ B_{ij} \right] &= 0 \\
\mathbb{E}\left[ (B_{ij} )^2 \right] & = \frac{\sigma^2}{S} \\
\mathbb{E}\left[ B_{ij} B_{ji} \right] & = \rho \frac{\sigma^2}{S} \ ,
\end{split}
\end{equation}
we find that the non-diagonal terms of $\mat{T}$ go to zero in expectation.
By introducing the notation
\begin{equation}
\quat{T}_{ii}  \equiv
\left(
	\begin{array} {cc}
	\quat{G}_{ii} & \quat{T}^b_{ii} \\
	... & ...
	\end{array}
	\right) 
\end{equation}
and
\begin{equation}
\quat{T}_{\star} \equiv \frac{1}{S} \sum_i \quat{T}_{ii} =
\left(
	\begin{array} {cc}
	\G & \quat{T}^b_{\star} \\
	... & ...
	\end{array}
	\right) \ ,
\end{equation}
the cavity equations for the diagonal terms read
\begin{equation}
\quat{T}_{ii} 
= \left(
	\begin{array} {cc}
	-\quat{q} & X_{ii} \\
	- D_{ii} - \quat{t} \circ \quat{T}^b_\star & \mat{I}
	\end{array}
	\right)^{-1}
\end{equation}
where $\quat{T}_\star = \sum_i \quat{T}_{ii} / S $,
while $\bf{t} = \sigma^2\rho + \sigma^2j$ with $\circ$ denoting the element-by-element matrix product (see section~\ref{sec:quaternions}).



We obtain a system of $2S$ quaternionic equations
\begin{equation}
\begin{cases}
\displaystyle
\quat{T}^b_{ii} = - X_{ii} \left(-\quat{q} -X_{ii} \left( - D_{ii} - \quat{t} \circ \quat{T}^b_\star \right) \right)^{-1} \\
\displaystyle
\quat{G}_{ii} =   \left( - \quat{q} - X_{ii} \left(- D_{ii} - \quat{t} \circ \quat{T}^b_\star  \right) \right)^{-1} \ ,
\end{cases}
\end{equation}
where 
\begin{equation}
\quat{T}^b_\star = \frac{1}{S} \sum_i \quat{T}_{ii} \ .
\end{equation}

Assuming that the elements of $\mat{X}$ and $\mat{D}$ are drawn from a given joint distirbution $P_{XD}$, we obtain the following 
two equations
\begin{equation}
\begin{cases}
\displaystyle
\quat{T}^b_{\star} = - \int dx \ ds \ P_{XD}(x,s)  x \left(-\quat{q} + x (  s + \quat{t} \circ \quat{T}^b_\star ) \right)^{-1} \\
\displaystyle
\G = \int dx \ ds \ P_{XD}(x,s)  \left( - \quat{q} + x ( s + \quat{t} \circ \quat{T}^b_\star ) \right)^{-1} \ .
\end{cases}
\label{eq:tbstar}
\end{equation}


At this point one can use
 $\quat{T}^b_{\star}= \alpha^b + \beta^b j $ and $\G = \alpha + \beta j$
to obtain $4$ equations of complex variables. It is simple to observe that $\beta^b = 0$ is always a solution and
$\beta = 0$ if and only if $\beta^b = 0$. The solution $\beta = \beta^b = 0$ always corresponds to a null specral density~\cite{XXX}.
The values of $\lambda$
for which a non-zero solution for $\beta$ exist correspond to the support of the spectral density.
Setting $\beta^b \neq 0$ and $\epsilon = 0$, one obtains from equation~\ref{eq:tbstar}
\begin{equation}
\begin{cases}
\displaystyle
\alpha^b =  \int dx \ ds \ P_{XD}(x,s)  
 \frac{ x \left( \bar{\lambda}  - s x - x \sigma^2 \rho \bar{\alpha}^b \right) }{
  | \lambda + x \left( -s-\alpha^b \rho\sigma^2 \right) | ^2 + | x\sigma^2 \beta^b |^2 } \\
\displaystyle
1 = \int dx \ ds \ P_{XD}(x,s)  \frac{ x^2 \sigma^2} {| \lambda + x \left( -s-\alpha^b \rho\sigma^2 \right) | ^2 + | x\sigma^2 \beta^b |^2 } \ .
\end{cases}\label{support:2}
\end{equation}

The values of $\lambda$ for which a solution of equations~\ref{support:2} exists are contained in the support
of the spectral density.
We assume that the solution $\beta^b$ of these equations vanishes at the boundaries of the support. In this case, the
points at the boundaries of the support of the spectral density are the complex solutions $\gamma$ of
\begin{equation}
\begin{cases}
\displaystyle
\alpha^b +  \rho \bar{\alpha}^b  =  \int dx \ ds \ P_{XD}(x,s)  
 \frac{ x \left( \bar{\gamma}  - s x \right) }{
  | \gamma + x \left( -s-\alpha^b \rho\sigma^2 \right) | ^2  } \\
\displaystyle
1 = \int dx \ ds \ P_{XD}(x,s)  \frac{ x^2 \sigma^2} {| \gamma + x \left( -s-\alpha^b \rho\sigma^2 \right) | ^2 } \ .
\end{cases}
\label{eq:support}
\end{equation}

\noindent We should note here that this method does not prove the
convergence in any mode of the spectral bulk,
but does yield a prediction for its support.

\section*{Support of the spectral density in the case $\rho = 0$}


In the case $\rho = 0$, the two equations~\ref{eq:support} become independent, and the support is defined by the solutions of
\begin{equation}
1 = \sigma^2 \int dx \ ds \ P_{XD}(x,s)  \frac{ x^2 } {| \gamma - s x  | ^2 } \ .
\end{equation} 
In the case $\sigma_d^2 = 0$, this equation further simplifies to
\begin{equation}
1 = \sigma^2 \int dx \ P_{X}(x)  \frac{ x^2 } {| \gamma - \mu_d x  | ^2 } \ . 
\end{equation} 
For instance, if $P_{X}$ is a uniform distribution on $[0,1]$, one can evaluate the integral, obtaining
\begin{equation}
\begin{aligned}
 1 & = \int_0^1 dx \ \frac{ x^2 \sigma^2} {| \gamma - \mu_d x | ^2 } \\
   &= \frac{ \sigma ^ 2} { \mu_d^3} \left( 2 \gamma \mu_d - \mu_d ^ 2 + 2 \gamma (\mu_d - \gamma) \log \left| \frac{\gamma} {\gamma - \mu_d} \right| \right) \ .
 \end{aligned}
 \end{equation}
The complex solutions $\gamma$ of this equation define the boundary of the support of the spectral density.


\bibliographystyle{nature}
\bibliography{abdrndmat} 

%\section*{Stability}
%Now that we have developed a method for predicting the eigenvalue distribution for matrices of the form  $\mat{M = XA}$, we can analyze the ability of the matrix $\mat{X}$ to destabilize the matrix $\mat{M}$ given a stable matrix $\mat{A}$. We expect varying $\mat{X}$ continuously to produce a continuous change in the eigenvalue distribution given by $\lambda$ in the above cavity method equations. Therefore, if varying $\mat{X}$ can produce an unstable $\mat{M}$, we must have an $\mat X$ for which $\lambda = 0$. We will show that if $\lambda = 0$, then the above cavity method equations require that the original matrix $\mat{A}$ was unstable. If $\lambda = 0$ and $\sigma_d^2 = 0$, we have 
%$$
%a = \frac{\mu_d - \sigma^2 \rho a} { \left(- \mu_d - a \rho \sigma^2 \right)^2 +b^2 \sigma^4}
%$$ and 
%$$1 =  \frac{ \sigma^2} { \left( -\mu_d - a \rho \sigma^2 \right)^2 +b^2 \sigma^4}
%$$ because these expressions are now independent of $X$. Solving for $a$ gives $ a = \frac {\mu_d} { \sigma^2 \left( 1 + \rho \right)}$ which then shows that $ 0 = \mu_d - \left( a + \rho \right) \sigma^2$. Therefore, it seems that a matrix $ \mat{M = X A}$ has a leading eigenvalue of 0 only if the matrix $\mat{A}$ has a leading eigenvalue of 0, independent of the choice of $\mat{X}$.

%We can confirm our analytical prediction by calculating the probability that $\mat{M}$ is unstable given $\mat{A}$. We first generate a large number of matrices $\mat{A}$ and compute their leading eigenvalue. Then, we subtract a diagonal matrix from $\mat{A}$ whose entries are the leading eigenvalue of $\mat{A}$ plus a positive constant $d$. Therefore, the resulting matrices $\mat{A}$ all have leading eigenvalue $d$. Then, we calculate the corresponding $\mat{M = XA}$ and compute their leading eigenvalues. We calculate the desired probability by dividing the number of unstable matrices $\mat{M}$ by the total number of matrices $\mat{M}$. We can also weight the conditional probability by the feasibility of the underlying growth rates by the formula $w \propto |\det(\mat{A})| |\mat{A}\vect{u}|^{-S/2}$ where $w$ is the weight and $\vect{u}$ is the vector from the diagonal of $\mat{X}$ normalized to unit length. In the weighted case, every destabilized $\mat M$ contributes to the probability proportional to its feasibility. In the text, we plot the conditional probability in both cases and observe that it tends towards zero as the size of the matrix $S$ increases, confirming our analytical prediction. In fact, the weighted probability tends toward zero faster than the unweighted. The following supplementary figures plot the conditional probability against increasing number of species for some parameter combinations not included in the main text. The first two plots show the ability of a population $\mat X$ to stabilize an unstable matrix $\mat A$. We calculated the probability of becoming stabilized using the same procedure as described above, except now the constant $d$ is negative so that the leading eigenvalue of $\mat A$ is positive. The remaining figures plot the conditional probability of $\mat M$ becoming unstable given a stable $\mat A$ as $S$ increases for distributions of $\mat X$ and distributions of the diagonal of $\mat A$ not included in the main text.

\begin{figure} [p]
	\includegraphics[scale = .75] {../../Code+Figures/suppFigDestabilized1.pdf}
	\caption{The conditional probability of a matrix $\mat M$ becoming \emph{stable} given an \emph{unstable} interaction matrix $\mat A$ with leading eigenvalue $-d$. The off-diagonal elements of $\mat A$ follow a bivariate normal distribution and, for each $S$, $\mu$ =0, $\sigma = 1 / \sqrt{S}$ and $\rho = -0.5$, $0$ or $0.5$. The elements of $\mat X$ are drawn from a uniform distribution on $(0,1)$, so that $\mu_X = 0.5$ and $\sigma_X^2 = \frac{1}{12}$. The diagonal elements of $\mat A$ are fixed at $-1$ since $\sigma_d^2 = 0$ and $\mu_D = -1$. Each probability is calculated from $1000$ trials from $1000$ simulated matrices $\mat{M} = \mat{XA}$.}
	\label{fig:destabilized1}
\end{figure}

\begin{figure}
	\includegraphics[scale = .75] {../../Code+Figures/suppFigDestabilized2.pdf}
	\caption{As in Figure \ref{fig:destabilized1}, except now the conditional probability is weighted by feasibility.}
	\label{fig:destabilized2}
\end{figure}

\begin{figure}
	\includegraphics[scale = .75] {../../Code+Figures/suppFigD1.pdf}
	\caption{The conditional probability of a matrix $\mat M$ becoming unstable given a stable interaction matrix $\mat A$ with leading eigenvalue $-d$. The off-diagonal elements of $\mat A$ follow a bivariate normal distribution and, for each $S$, $\mu$ =0, $\sigma = 1 / \sqrt{S}$ and $\rho = -0.5$, $0$ or $0.5$. The elements of $\mat X$ are drawn from a uniform distribution on $(0,1)$, so that $\mu_X = 0.5$ and $\sigma_X^2 = \frac{1}{12}$. The diagonal elements of $\mat A$ follow a uniform distribution on $(-0.75, -1.25)$ so that $\sigma_d^2 = \frac{1}{48}$ and $\mu_D = -1$. Each probability is calculated from $1000$ trials from $1000$ simulated matrices $\mat{M} = \mat{XA}$.}
	\label{fig:Dvarying1}
\end{figure}

\begin{figure}
	\includegraphics[scale = .75] {../../Code+Figures/suppFigD2.pdf}
	\caption{As in Figure \ref{fig:Dvarying1}, except now the conditional probability is weighted by feasibility.}
	\label{fig:Dvarying2}
\end{figure}


%\begin{figure}
%	\includegraphics[scale = .75] {../../Code+Figures/suppFigLogNormal1.pdf}
%	\caption{The conditional probability of a matrix $\mat M$ becoming unstable given an stable interaction matrix $\mat A$ with leading eigenvalue $-d$. The off-diagonal elements of $\mat A$ follow a bivariate normal distribution and, for each $S$, $\mu$ =0, $\sigma = 1 / \sqrt{S}$ and $\rho = -0.5$, $0$ or $0.5$. The elements of $\mat X$ are drawn from a log-normal distribution with log-mean and log-standard deviation both $0.5$ so that $\mu_X = \exp{5/8}$ and $\sigma_X^2 = \left(\exp{1/4} - 1 \right) \exp{5/4}$. The diagonal elements of $\mat A$ are fixed at $-1$ since $\sigma_d^2 = 0$ and $\mu_D = -1$. Each probability is calculated from $100$ trials from $100$ simulated matrices $\mat{M} = \mat{XA}$.}
%	\label{fig:Xlognormal1}
%\end{figure}

%\begin{figure}
%	\includegraphics[scale = .75] {../../Code+Figures/suppFigLogNormal2.pdf}
%	\caption{As in Figure \ref{fig:Xlognormal1}, except now the conditional probability is weighted by feasibility.}
%	\label{fig:Xlognormal2}
%\end{figure}

%\begin{figure}
%	\includegraphics[scale = .75] {../../Code+Figures/suppFigHalfNormal1.pdf}
%	\caption{The conditional probability of a matrix $\mat M$ becoming unstable given an stable interaction matrix $\mat A$ with leading eigenvalue $-d$. The off-diagonal elements of $\mat A$ follow a bivariate normal distribution and, for each $S$, $\mu$ =0, $\sigma = 1 / \sqrt{S}$ and $\rho = -0.5$, $0$ or $0.5$. The elements of $\mat X$ are drawn from a half-normal distribution shifted rightwards with support is $(1, \infty)$ and with theta parameter $\theta = 1$ so that $\mu_X = 2$ and $\sigma_X^2 = \pi/2 -1$. The diagonal elements of $\mat A$ are fixed at $-1$ since $\sigma_d^2 = 0$ and $\mu_D = -1$. Each probability is calculated from $100$ trials from $100$ simulated matrices $\mat{M} = \mat{XA}$.}
%	\label{fig:Xhalfnormal1}
%\end{figure}

%\begin{figure}
%	\includegraphics[scale = .75] {../../Code+Figures/suppFigHalfNormal2.pdf}
%	\caption{As in Figure \ref{fig:Xhalfnormal1}, except now the conditional probability is weighted by feasibility.}
%	\label{fig:Xhalfnormal2}
%\end{figure}


\end{document}


