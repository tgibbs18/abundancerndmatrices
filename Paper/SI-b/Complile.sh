cp ~/Dropbox/library.bib abdrndmat.bib

#rubber -f --inplace -n -1 -d -W all $1.tex
#rubber --clean $1.tex

pdflatex $1.tex
pdflatex $1.tex
pdflatex $1.tex
bibtex $1
pdflatex $1.tex
pdflatex $1.tex
pdflatex $1.tex

rm *.aux *.log *.blg *.bbl

evince $1.pdf &
