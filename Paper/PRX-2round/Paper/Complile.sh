cp ~/Dropbox/library.bib abdrndmat.bib

#rubber -f --inplace -n -1 -d -W all $1.tex
#rubber --clean $1.tex

pdflatex $1.tex
pdflatex $1.tex
pdflatex $1.tex
bibtex $1
pdflatex $1.tex
pdflatex $1.tex
pdflatex $1.tex

latexdiff ../../PRX/Gibbs_AbdRndMat.tex $1.tex > diff.tex

pdflatex diff.tex
pdflatex diff.tex
pdflatex diff.tex
bibtex diff
pdflatex diff.tex
pdflatex diff.tex
pdflatex diff.tex



rm diff.tex
mv diff.pdf GibbsEtAl-DiffMain.pdf

rm *.aux *.log *.blg *.bbl

evince GibbsEtAl-DiffMain.pdf &
