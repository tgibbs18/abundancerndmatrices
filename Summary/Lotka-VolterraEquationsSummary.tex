\documentclass [12pt] {article}
\title{Supplementary Notes}
\author{}

\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}

\newcommand{\vect}[1]{\boldsymbol{#1}}
\newcommand{\mat}[1]{\boldsymbol{#1}}
\DeclareMathOperator*{\tr}{\text{tr}}


\begin{document}
\maketitle

\section*{Aims}

We aim to analyze the stability, and hence eigenvalue distribution, of a matrix $\mat{M}$ of the form $\mat{M = XA}$. $\mat{X}$ is a diagonal matrix with diagonal entries drawn from an arbitrary distribution with positive support mean $\mu_X$ and variance $\sigma_X^2$. The diagonal entries of $\mat A$ are drawn from an arbitrary distribution with negative support, mean $\mu_d$ and variance $\sigma_d^2$. Each pair of off-diagonal entries $(A_{ij}, A_{ji})$ are drawn from a bivariate distribution with identical marginal mean $\mu$, variances $\sigma^2$ and correlation $\rho$. First, we will introduce quaternions and the relationship between the spectral density of a random matrix and the resolvent. Then, we will analyze the affect of a non-zero mean $\mu$. Next, we will derive a prediction for the support of the eigenvalue distribution of $\mat M$ from the cavity method. Lastly, we will investigate if, given a stable matrix $\mat A$, the matrix $\mat M$ can be destabilized because of a population matrix $\mat X$.


\section*{Quaternions}

When constructing the complex numbers from the real numbers, one defines a variable $i$ to be a root of the equation $x^2 + 1 = 0$. The algebraic structure of $\mathbb{C}$ descends from the equation $i^2 = -1$ and the algebraic structure of $\mathbb{R}$. If instead the symbols $i$, $j$ and $k$ are defined by the relations 
$$ i^2 = j^2 = k^2 = ijk = -1, $$ the resuting algebraic structure is a 4-dimensional extension of the complex numbers called the quaternions. Quaternionic multiplication is not commutative. A quaternion $\bf q$ can be written 
$$ \textbf{q} = a + bi + cj + dk = z + wj$$ where $a,b,c,d \in \mathbb{R}$ and $z = a + bi$ and $w = c + di$ are complex. $\textbf{q}$ can also be written in matrix form $$ \textbf{q} = \left( \begin{matrix} z & w \\ \bar{w} & \bar{z} \\ \end{matrix} \right)$$ with $z$ and $w$ as before. The conjugate of a quaternion $\textbf{q} = z + wj$ is defined to be $\bar{\textbf{q}} = z - wj$, so that its inverse is $$ \bf q^{-1} = \frac{\bar{q}}{|q|^2}$$ as in the case for complex numbers. The inverse of $ \bf q $ can also be computed by finding the inverse of the corresponding matrix.

\section*{Spectral Distribution and Resolvent}

Let $ \mat  B $ be an $ S \times S $ random matrix with eigenvalues $ \lambda_i$ for $i = 1, ... , S$. Since $\mat B $ is not necessarily Hermitian, its eigenvalues may be complex. Therefore, the spectral density is defined as 
$$ \varrho(\lambda) = \frac{1} {S} \sum_{i=1}^S\delta(\Re(\lambda) - \Re(\lambda_i))\delta(\Im(\lambda) - \Im(\lambda_i)) $$ which, in expectation, converges to 
$$ \varrho(\lambda) = \mathbb{E} [\delta(\Re(\lambda) - \Re(\lambda_i))\delta(\Im(\lambda) - \Im(\lambda_i)) ] $$ as $S$ gets large. We aim to calculate the limiting distribution of $\varrho(\lambda)$ to find the stability of the matrix $\bf M$. We introduce the resolvent defined by 
$$ G(\textbf{q}; \mat{B}) = \frac{1} {S} \sum_{i=1}^S (\lambda_i - \textbf{q})^{-1}$$ where $\textbf{q}$ is a quaternion. In the limit of large $S$, we have 
$$ G(\textbf{q}; \mat{B}) = \mathbb{E} [\frac{1} {S}  \tr \left( \textbf{q} - \mat{B} \right)^{-1}] $$ In the case of Hermitian matrices, the resolvent is a complex function because all eigenvalues are real. However, in our case it will be a quaternionic function.

The resolvent is useful because it's simply related to the spectral density by the following two formulas:

\begin{equation}
G(\textbf{q}; \mat{B}) = \int \varrho(\lambda) (\lambda - \textbf{q})^{-1} d\lambda
\label{resolvent+spectrum}
\end{equation}
and \begin{equation}
\varrho\left(\lambda \right) =  - \frac {1} {S \pi} \lim_{\epsilon \to 0} \Re \left( \frac{\partial} {\partial \bar{ \lambda} }  \sum_{i} G(\lambda + j \epsilon; \mat{B})  \right).
\end{equation}

\section*{The Bulk of $\mat{M}$}

We want to show that the mean of $\bf{A}$ only affects the eigenvalue distribution of $\bf{M}$ by changing one outlying eigenvalue. We decompose the matrix $\bf{M}$ as 
$$ \mat{M} = \mat{X} (\mat{D} - \mu \mat{I} + \mat{B} + \mu \mat{1} ) $$ where $\mat{I}$ is the identity matrix, $\mat{1}$ is a matrix of ones and $\mat D $ is the diagonal matrix consisting of the diagonal entries of $ \mat A $. We will show that the bulk of the spectrum of $\mat M$ is determined by the eigenvalues of the matrix $ \mat{J} =  \mat{X} (\mat{D} - \mu \mat{I}+ \mat{B}) $. The resolvent of the decomposed matrix $\mat M$ is 
\begin{equation} 
\begin{aligned}
G(\textbf{q}; \mat{M}) &= \mathbb{E} [\frac{1} {S} \tr \left( \textbf{q} - \mat{X} \mat{D} - \mu \mat{X} - \mat{X} \mat{B} - \mu \mat{X1} \right)^{-1}] \\
 &= \mathbb{E} [\frac{1} {S} \tr \left( \textbf{q} - \mat{J} - \mu \mat{X1} \right)^{-1}].\\
 \end{aligned}
 \end{equation}
 So, we want to compute the inverse of the above matrix. Ken Miller [Ken Miller in 1981 \ref{}] proved that if $\mat{Y}$ and $\mat{Y + Z}$ are invertible matrices and $\mat{Z}$ is rank 1, then 
$$(\mat{Y + Z})  ^ {-1} = \frac{1} {1+ g}  \mat{Y^{-1} Z Y^{-1}}$$ where $ g = \tr (\mat{ZY^{-1}})$. Since $\mu \mat{X1}$ is a rank 1 matrix, we have
\begin{equation}
 \left( \textbf{q} - \mat{J} - \mu \mat{X1} \right)^{-1} = (\textbf{q} - \mat{J})^{-1} + \frac{1} {1+ g} (\textbf{q} - \mat{J})^{-1} \mu \mat{X1} (\textbf{q} - \mat{J})^{-1}
 \label{inverse}
 \end{equation}
  where $g = \tr (\mu \mat{X1} (\textbf{q} - \mat{J})^{-1})$. $g$ scales with $S$ in expectation since it involves the trace of the $S \times S$ matrix. Now, let's introduce the linear operator $\langle \cdot \rangle$ defined by $\langle \mat {C} \rangle = \mathbb{E} [\frac{1} {S} \tr {\mat{C}} ]$ for an $S \times S $ matrix $\mat C$. Applying $\langle \cdot \rangle$ to both sides of \ref{inverse} we obtain
 \begin{equation}
 \begin{aligned}
 G(\textbf{q}; \mat{M}) & = G(\textbf{q}; \mat{J}) + \langle \frac{1} {1+ g} (\textbf{q} - \mat{J})^{-1} \mu \mat{X1} (\textbf{q} - \mat{J})^{-1} \rangle \\
  & \approx G(\textbf{q}; \mat{J}) +  \frac{1} {1 + S \alpha} \langle (\textbf{q} - \mat{J})^{-1} \mu \mat{X1} (\textbf{q} - \mat{J})^{-1} \rangle \\
  \label{subleading}
\end{aligned}
\end{equation}
where $\alpha$ is the mean of the diagonal entries of the matrix $\mu \mat{X1} (\textbf{q} - \mat{J})^{-1}$. In the limit of large $S$, the contribution from the second term in \ref{subleading} goes to zero since $\alpha$ is constant. Therefore, the resolvent of $\mat M$ is about the same as the resolvent of $\mat J$ when $S$ is large. In other words, the bulk of the eigenvalues of $\mat M$ and $\mat J$ match, as shown in Figure \ref{fig:M+J}. In the text, we observed that the eigenvalue distribution of $\mat M$ is characterized by a bulk of eigenvalues and one outlying eigenvalue. Since the bulk of eigenvalues are determined by $ \mat J $, we account for the contribution of the bulk on $\tr \mat M$ to obtain the prediction $\lambda_{out} = \mu_X (\mu_d + (S - 1)\mu)$ derived in the text.

\begin{figure}
	\includegraphics[scale = .75] {../Code+Figures/suppFigM+J.pdf}
	\caption{The eigenvalue distributions for $\mat M $ and $ \mat J $ of size $ S = 250 $ with $\mat D $ and $\mat X$  following different distributions. In each case, $\mat A $ is a bivariate normal distribution with identical marginals $ \mu = 2 / S$, $\sigma = 0.5$ and $ \rho = 0$. Uniform D has $\mat D $ following a uniform distribution on $(-1.5, -0.5)$. LogNormal X has $\mat X$ following a log-normal distribution with log-mean $0$ and log-standard deviation $0.35$. HalfNormal X has $ \mat X $ following a half-normal distribution with support $(1, \infty)$ with parameter $\theta = 1$.}
	\label{fig:M+J}
\end{figure}

\section*{The Spectrum of $\mat{Q}$}

If the interaction matrix $\mat A = \mat{D} - \mu \mat{I} + \mat{B} + \mu \mat{1} $ has a non-zero off-diagonal mean, then its eigenvalue distribution has an outlier predicted by the rank 1 matrix $ (\mu_d - \mu) \mat{I}+ \mu \mat{1} $. In the text, we observed that the outlying eigenvalue of $ \mat{M} = \mat{X} ( \mat{D} - \mu \mat{I}+ \mat{B} + \mu \mat{1} ) $ is determined by the outlying eigenvalue of $\mat{Q} = \mat{X} \left(  \mat{D} - \mu \mat{I} + \mu \mat{1} \right)$. Since $\mat Q $ is not rank 1, its eigenvalues other than the outlier will not all be zero. If we take $ \mat{J} =  \mat{X} ( \mat{D} - \mu \mat{I} + \mat{B}) $ as before and set $\sigma = 0$ then $\mat B $ is a matrix of zeroes so that $\mat {M} = \mat{Q}$ and $ \mat{J} =  \mat{X} ( \mat{D} - \mu \mat{I})$.  The spectrum of $\mat{J}$ will be the distribution of non-outlying eigenvalues of $\mat Q$ because we showed that the bulk of $\mat M$ is determined by $\mat{J}$. The resolvent of $\mat J$ is 
\begin{equation}
\begin{aligned}
G(\textbf{q}; \mat{J}) & = \frac{1} {S} \tr \left( \textbf{q} - \mat{J} \right)^{-1} \\ 
& = \frac{1} {S} \tr \left( \textbf{q} - \mat{X} ( \mat{D} - \mu \mat{I})  \right)^{-1} \\
& = \frac{1} {S} \sum_{i = 1}^S \frac{1} {q - X_iD_i -+\mu X_i} \\
\label{resolventQ}
\end{aligned}
\end{equation}
since $\textbf{q} - \mat{X} ( \mat{D} - \mu \mat{I})$ is symmetric. In the limit of large $S$, the sum in \ref{resolventQ} tends toward $ \mathbb{E} \left[(\textbf{q} - \mat{X} ( \mat{D} - \mu \mat{I}))^{-1} \right]$. If $\beta(X)$ is the distribution of $\mat X$ and $\gamma(D)$ is the distribution of $\mat D$, we have 
$$
\begin{aligned}
G(\textbf{q}; \mat{J}) & = \frac{1} {S} \sum_{i = 1}^S \frac{1} {q - X_iD_i + \mu X_i} \\
& = \mathbb{E} \left[(\textbf{q} - \mat{X} ( \mat{D} - \mu \mat{I}))^{-1} \right] \\
& = \int \int \frac{\beta(X)\gamma(D)} {q - XD + \mu X} dX dD. \\
\label{spectrumQ}
\end{aligned}
$$
In the particular case of $\sigma_d = 0$, the equation simplifies to
$$
\begin{aligned}
G(\textbf{q}; \mat{J}) &= \int \frac{\beta(X)} {q - (\mu_d-\mu)X} dX \\
&= \frac{1}{\mu_d - \mu}\int \frac{\beta \left(\frac{Y} { \mu_d - \mu} \right)} {q - Y} dY \\
\end{aligned}
$$
with the change of variables $Y = (\mu_d - \mu) X$. But from Equation \ref{resolvent+spectrum}, we know the spectral density $\varrho(\lambda)$ for $\mat J$ must be 
\begin{equation}
\varrho(\lambda) =  \frac{1}{\mu_d - \mu} \beta \left(\frac{\lambda} { \mu_d - \mu} \right).
\label{predictedQ}
\end{equation}
In Figure \ref{fig:observation}, we plot the prediction from Equation \ref{predictedQ} against the eigenvalue distribution of $\mat Q $ for two distributions of $\mat X$.


\begin{figure}
	\includegraphics[scale = .75] {../Code+Figures/suppFigSpectrumB.pdf}
	\caption{Histograms of the eigenvalue distribution for two matrices $\mat Q$ of size $ S = 1500$. In each matrix, $\sigma_d = 0$, $\mu_d = -1$ and $ \mu = 5 / S$. The Uniform plot has $\mat{X}$ following a uniform distribution on $(0,1)$ and the LogNormal plot has $\mat{X}$ following a log-normal distribution with log-mean and log-standard deviation both $0.5$.}
	\label{fig:observation}
\end{figure}


\section*{Cavity Method}
We want to find the eigenvalue distribution of the matrix $\mat {M = XA}$. In the previous section, we showed that we can assume $\mu = 0$. Let $\mat D$ be the diagonal matrix from the diagonal of $\mat A$ as before. From the usual quaternionic cavity method, we write
$$
\varrho\left(\lambda; \mat{M}\right) =  - \frac {1} {S \pi} \lim_{\epsilon \to 0} \Re \left( \frac{\partial} {\partial \bar{ \lambda} }  \sum_{i}  \mat{R}_{ii,11} \right)
$$
for the spectral density, where $\mat{R = \left(M - q \right)}^{-1}$ is the resolvent matrix for doubled matrices
$$ \mat{M} = \left( \begin{matrix} 
M_{ij} & 0 \\
0 & M_{ji} 
\end{matrix} \right)
\qquad
\bf{q} = \left( \begin{matrix} 
\lambda & i\epsilon \\
i\epsilon & \bar{\lambda}
\end{matrix} \right)
$$

Now, let
$$ \mat{T} = \left( \begin{matrix}
-\bf{q} & \mat{X} \\
\mat{- A} & \mat{I}
\end{matrix} \right) ^{-1} 
= \left( \begin{matrix} 
\mat{R} & ... \\
... & ...
\end{matrix} \right) $$
so that $\mat{T}$ is again a doubled (block structured) matrix. We also have that $\mat{R_{ii,11} = T_{ii,11}}$ by the construction of $\mat{T}$. Let $\mat{0}$ be matrix all of whose entires are $0$ and let $\mat{A_{ij}} = \left( \begin{matrix} A_{ij} & 0 \\ 0 & A_{ji} \end{matrix} \right) $. We can write $\mat{T}$ as $S^2$ blocks, and, following the usual cavity method procedure, we have 
$$
\mat{T_{ii}} = \left[ \left( \begin{matrix}
-\mat{q} & \mat{X} \\
- \mat{D} & \mat{I} 
\end{matrix} \right)
- \sum_{j,k \neq i }
\left( \begin{matrix}
\mat{0} & \mat{0} \\
\mat{A_{ij}} & \mat{0}
\end{matrix} \right)
\mat{T}_{jk}^{(i)}
\left( \begin{matrix}
\mat{0} & \mat{0} \\
\mat{A_{ki}} & \mat{0}
\end{matrix} \right)
\right]^{-1}
$$
along the diagonal since we've doubled the size of $\mat{T}$ twice. Let $S$ tend to infinity so that we can make three observations. First, the effect removing one site from the matrix is small when $S$ is large so we can take $\mat{T_{jk}^{(i)} = T_{jk}}$. We can apply the law of large number to take the expectation of the matrices and have two further simplifications. This expectation is over $\mat{A}$, $\mat{X}$ and $\mat{D}$. Because of our assumptions about $\mat{A}$, we know that the non-diagonal terms go to zero in expectation. We also know that $\bf{T_{ii}}$ is, in expectation, the same for each $i$, so we write $\mat{T_{ii} = T_*}$ instead. Finally, we let $\epsilon$ tend to zero in $\mat{T_*}$ and introduce $\mat{T^b} = a + bj$ given by 
\begin{equation}
\mat{T_*} = \mathbb{E}\left[
	\begin{array} {cc}
	-\bf{q} & \mat{X} \\
	- \mat{D} - t\circ\bf{T^b} & \mat{I}
	\end{array}
	\right]^{-1}
	= \mathbb{E}\left[
	\begin{array} {cc}
	\mat{R} & \mat{T^b} \\
	... & ...
	\end{array}
	\right]
\end{equation}
with $\bf{t} = \sigma^2\rho + \sigma^2j$
	and $\circ$ denoting the element-by-element matrix product.\newline


We can calculate 
$$
\mat{T^b} = \mathbb{E}\left[ -\left(-q-X\left( - D-t \circ T^b \right) \right)^{-1}X\right]
$$ and 
$$
\mat{R} = \mathbb{E} \left[ \left( -q - X\left(- D - t \circ T^b \right) \right) ^{-1} \right]
$$ to try to solve for the support by finding $\bf{T_*}$ and hence the resolvent.
Now, split $T^b$ into imaginary and quaternionic parts to yield
\begin{equation}
a = \mathbb{E} \left[ \frac{ X \bar{\lambda} - D X^2 - X^2 \sigma^2 \rho \bar{a}} {| \lambda + X \left( -D-a\rho\sigma^2 \right) | ^2 + | X\sigma^2 b|^2 } \right]
\end{equation}
and 
\begin{equation} \label {support:2}
1 =  \mathbb{E} \left[ \frac{ X^2 \sigma^2} {| \lambda + X \left(- D-a\rho\sigma^2 \right) | ^2 + | X\sigma^2 b|^2 } \right]
\end{equation} 
assuming $b \neq 0$. \newline

\section*{Uncorrelated Support Predictions}
First, let's solve for the support in the simplest case of $\rho = 0$. Equation \ref{support:2}  yields a prediction for the support
\begin{equation}
1 =  \mathbb{E} \left[ \frac{ X^2 \sigma^2} {| \lambda - DX | ^2 } \right]
\end{equation} 
For the explicit case of $X$ following a uniform distribution on $(0,1)$ and $\sigma_d^2 = 0$, we can calculate the above expectation and find an expression for all $\lambda$ in the support. 
\begin{equation}
\begin{aligned}
 1 & = \mathbb{E} \left[ \frac{ X^2 \sigma^2} {| \lambda - \mu_d X | ^2 } \right] \\
   & = \int_0^1  \frac{ X^2 \sigma^2} {| \lambda - \mu_d X | ^2 } dX \\
   &= \frac{ \sigma ^ 2} { \mu_d^3} \left( 2 \lambda \mu_d - \mu_d ^ 2 + 2 \lambda (\mu_d - \lambda) \log \left| \frac{\lambda} {\lambda - \mu_d} \right| \right)
 \end{aligned}
 \end{equation}
If we let $X$ follow the log-normal distribution and let $\sigma_d^2 = 0$, we can integrate the following expression numerically and obtain predictions for the support as well.

\begin{equation}
\mathbb{E} \left[ \frac{ X^2 \sigma^2} {| \lambda - \mu_d X | ^2 } \right]  = \int_0^\infty  \frac{ X^2 \sigma^2 \frac {1} {\sqrt{2\pi}\sigma_l X} e^\frac {- \left(\ln{X}-\theta \right)^2} {2\sigma_l^2}} {| \lambda - \mu_d X | ^2 } dX
\end{equation} where $\theta$ is the location parameter and $\sigma_l$ is the scale parameter of the log-normal distribution.

\section*{Stability}
Now that we have developed a method for predicting the eigenvalue distribution for matrices of the form  $\mat{M = XA}$, we can analyze the ability of the matrix $\mat{X}$ to destabilize the matrix $\mat{M}$ given a stable matrix $\mat{A}$. We expect varying $\mat{X}$ continuously to produce a continuous change in the eigenvalue distribution given by $\lambda$ in the above cavity method equations. Therefore, if varying $\mat{X}$ can produce an unstable $\mat{M}$, we must have an $\mat X$ for which $\lambda = 0$. We will show that if $\lambda = 0$, then the above cavity method equations require that the original matrix $\mat{A}$ was unstable. If $\lambda = 0$ and $\sigma_d^2 = 0$, we have 
$$
a = \frac{\mu_d - \sigma^2 \rho a} { \left(- \mu_d - a \rho \sigma^2 \right)^2 +b^2 \sigma^4}
$$ and 
$$1 =  \frac{ \sigma^2} { \left( -\mu_d - a \rho \sigma^2 \right)^2 +b^2 \sigma^4}
$$ because these expressions are now independent of $X$. Solving for $a$ gives $ a = \frac {\mu_d} { \sigma^2 \left( 1 + \rho \right)}$ which then shows that $ 0 = \mu_d - \left( a + \rho \right) \sigma^2$. Therefore, it seems that a matrix $ \mat{M = X A}$ has a leading eigenvalue of 0 only if the matrix $\mat{A}$ has a leading eigenvalue of 0, independent of the choice of $\mat{X}$.

We can confirm our analytical prediction by calculating the probability that $\mat{M}$ is unstable given $\mat{A}$. We first generate a large number of matrices $\mat{A}$ and compute their leading eigenvalue. Then, we subtract a diagonal matrix from $\mat{A}$ whose entries are the leading eigenvalue of $\mat{A}$ plus a positive constant $d$. Therefore, the resulting matrices $\mat{A}$ all have leading eigenvalue $d$. Then, we calculate the corresponding $\mat{M = XA}$ and compute their leading eigenvalues. We calculate the desired probability by dividing the number of unstable matrices $\mat{M}$ by the total number of matrices $\mat{M}$. We can also weight the conditional probability by the feasibility of the underlying growth rates by the formula $w \propto |\det(\mat{A})| |\mat{A}\vect{u}|^{-S/2}$ where $w$ is the weight and $\vect{u}$ is the vector from the diagonal of $\mat{X}$ normalized to unit length. In the weighted case, every destabilized $\mat M$ contributes to the probability proportional to its feasibility. In the text, we plot the conditional probability in both cases and observe that it tends towards zero as the size of the matrix $S$ increases, confirming our analytical prediction. In fact, the weighted probability tends toward zero faster than the unweighted. The following supplementary figures plot the conditional probability against increasing number of species for some parameter combinations not included in the main text. The first two plots show the ability of a population $\mat X$ to stabilize an unstable matrix $\mat A$. We calculated the probability of becoming stabilized using the same procedure as described above, except now the constant $d$ is negative so that the leading eigenvalue of $\mat A$ is positive. The remaining figures plot the conditional probability of $\mat M$ becoming unstable given a stable $\mat A$ as $S$ increases for distributions of $\mat X$ and distributions of the diagonal of $\mat A$ not included in the main text.

\begin{figure} [p]
	\includegraphics[scale = .75] {../Code+Figures/suppFigDestabilized1.pdf}
	\caption{The conditional probability of a matrix $\mat M$ becoming \emph{stable} given an \emph{unstable} interaction matrix $\mat A$ with leading eigenvalue $-d$. The off-diagonal elements of $\mat A$ follow a bivariate normal distribution and, for each $S$, $\mu$ =0, $\sigma = 1 / \sqrt{S}$ and $\rho = -0.5$, $0$ or $0.5$. The elements of $\mat X$ are drawn from a uniform distribution on $(0,1)$, so that $\mu_X = 0.5$ and $\sigma_X^2 = \frac{1}{12}$. The diagonal elements of $\mat A$ are fixed at $-1$ since $\sigma_d^2 = 0$ and $\mu_D = -1$. Each probability is calculated from $1000$ trials from $1000$ simulated matrices $\mat{M} = \mat{XA}$.}
	\label{fig:destabilized1}
\end{figure}

\begin{figure}
	\includegraphics[scale = .75] {../Code+Figures/suppFigDestabilized2.pdf}
	\caption{As in Figure \ref{fig:destabilized1}, except now the conditional probability is weighted by feasibility.}
	\label{fig:destabilized2}
\end{figure}

\begin{figure}
	\includegraphics[scale = .75] {../Code+Figures/suppFigD1.pdf}
	\caption{The conditional probability of a matrix $\mat M$ becoming unstable given an stable interaction matrix $\mat A$ with leading eigenvalue $-d$. The off-diagonal elements of $\mat A$ follow a bivariate normal distribution and, for each $S$, $\mu$ =0, $\sigma = 1 / \sqrt{S}$ and $\rho = -0.5$, $0$ or $0.5$. The elements of $\mat X$ are drawn from a uniform distribution on $(0,1)$, so that $\mu_X = 0.5$ and $\sigma_X^2 = \frac{1}{12}$. The diagonal elements of $\mat A$ follow a uniform distribution on $(-0.75, -1.25)$ so that $\sigma_d^2 = \frac{1}{48}$ and $\mu_D = -1$. Each probability is calculated from $1000$ trials from $1000$ simulated matrices $\mat{M} = \mat{XA}$.}
	\label{fig:Dvarying1}
\end{figure}

\begin{figure}
	\includegraphics[scale = .75] {../Code+Figures/suppFigD2.pdf}
	\caption{As in Figure \ref{fig:Dvarying1}, except now the conditional probability is weighted by feasibility.}
	\label{fig:Dvarying2}
\end{figure}

\begin{figure}
	\includegraphics[scale = .75] {../Code+Figures/suppFigLogNormal1.pdf}
	\caption{The conditional probability of a matrix $\mat M$ becoming unstable given an stable interaction matrix $\mat A$ with leading eigenvalue $-d$. The off-diagonal elements of $\mat A$ follow a bivariate normal distribution and, for each $S$, $\mu$ =0, $\sigma = 1 / \sqrt{S}$ and $\rho = -0.5$, $0$ or $0.5$. The elements of $\mat X$ are drawn from a log-normal distribution with log-mean and log-standard deviation both $0.5$ so that $\mu_X = \exp{5/8}$ and $\sigma_X^2 = \left(\exp{1/4} - 1 \right) \exp{5/4}$. The diagonal elements of $\mat A$ are fixed at $-1$ since $\sigma_d^2 = 0$ and $\mu_D = -1$. Each probability is calculated from $100$ trials from $100$ simulated matrices $\mat{M} = \mat{XA}$.}
	\label{fig:Xlognormal1}
\end{figure}

\begin{figure}
	\includegraphics[scale = .75] {../Code+Figures/suppFigLogNormal2.pdf}
	\caption{As in Figure \ref{fig:Xlognormal1}, except now the conditional probability is weighted by feasibility.}
	\label{fig:Xlognormal2}
\end{figure}

\begin{figure}
	\includegraphics[scale = .75] {../Code+Figures/suppFigHalfNormal1.pdf}
	\caption{The conditional probability of a matrix $\mat M$ becoming unstable given an stable interaction matrix $\mat A$ with leading eigenvalue $-d$. The off-diagonal elements of $\mat A$ follow a bivariate normal distribution and, for each $S$, $\mu$ =0, $\sigma = 1 / \sqrt{S}$ and $\rho = -0.5$, $0$ or $0.5$. The elements of $\mat X$ are drawn from a half-normal distribution shifted rightwards with support is $(1, \infty)$ and with theta parameter $\theta = 1$ so that $\mu_X = 2$ and $\sigma_X^2 = \pi/2 -1$. The diagonal elements of $\mat A$ are fixed at $-1$ since $\sigma_d^2 = 0$ and $\mu_D = -1$. Each probability is calculated from $100$ trials from $100$ simulated matrices $\mat{M} = \mat{XA}$.}
	\label{fig:Xhalfnormal1}
\end{figure}

\begin{figure}
	\includegraphics[scale = .75] {../Code+Figures/suppFigHalfNormal2.pdf}
	\caption{As in Figure \ref{fig:Xhalfnormal1}, except now the conditional probability is weighted by feasibility.}
	\label{fig:Xhalfnormal2}
\end{figure}


\end{document}


